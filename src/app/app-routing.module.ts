import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InicioComponent } from './inicio/inicio.component';
import { IniciarSesionComponent } from './iniciar-sesion/iniciar-sesion.component';
import { RegistroSesionComponent } from './registro-sesion/registro-sesion.component';

const routes: Routes = [ 
  {path:"", redirectTo: "inicio", pathMatch:"full"},
  {path:"iniciar-sesion",component:IniciarSesionComponent},
  {path:"registro-sesion",component:RegistroSesionComponent},
  {path:"inicio",component: InicioComponent},
  {path:"**", redirectTo:"inicio", pathMatch:"full"},
]


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
